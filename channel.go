package deque

import "sync"

// Channel like chan but use deque, pop will block if deque empty,push will block if deque full
type Channel struct {
	channel *ChannelRead
	cond    *sync.Cond
	wait    int
	closed  bool
}

// NewChannel .
func NewChannel(deque Deque) *Channel {
	return &Channel{
		channel: NewChannelRead(deque),
		cond:    sync.NewCond(&sync.Mutex{}),
	}
}

// Close close channel push and pop will return
func (c *Channel) Close() (e error) {
	c.cond.L.Lock()
	if c.closed {
		e = ErrChannelClosed
	} else {
		c.closed = true
		c.channel.Close()
		if c.wait != 0 {
			c.cond.Broadcast()
		}
	}
	c.cond.L.Unlock()
	return
}

// PushBack append value to end
func (c *Channel) PushBack(value Value) (ok bool, e error) {
	c.cond.L.Lock()
	for {
		if c.closed {
			e = ErrChannelClosed
			break
		}
		ok, e = c.channel.PushBack(value)
		if e != nil {
			break
		}
		if ok {
			break
		}

		c.wait++      // add wait
		c.cond.Wait() // wait work
		c.wait--      // sub wait
	}
	c.cond.L.Unlock()
	return
}

// PushFront insert value before of first
func (c *Channel) PushFront(value Value) (ok bool, e error) {
	c.cond.L.Lock()
	for {
		if c.closed {
			e = ErrChannelClosed
			break
		}
		ok, e = c.channel.PushFront(value)
		if e != nil {
			break
		}
		if ok {
			break
		}

		c.wait++      // add wait
		c.cond.Wait() // wait work
		c.wait--      // sub wait
	}
	c.cond.L.Unlock()
	return
}

// PopFront if empty wait, else erase and return first value
func (c *Channel) PopFront() (value Value, e error) {
	value, e = c.channel.PopFront()
	if e == nil {
		c.cond.L.Lock()
		if c.wait != 0 {
			c.cond.Signal()
		}
		c.cond.L.Unlock()
	}
	return
}

// PopBack if empty wait, else erase and return last value
func (c *Channel) PopBack() (value Value, e error) {
	value, e = c.channel.PopBack()
	if e == nil {
		c.cond.L.Lock()
		if c.wait != 0 {
			c.cond.Signal()
		}
		c.cond.L.Unlock()
	}
	return
}
