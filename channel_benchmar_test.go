package deque_test

import (
	"sync"
	"testing"

	"gitlab.com/king011_go/deque"
)

func BenchmarkGoChan1ToN(b *testing.B) {
	ch := make(chan int, 100)
	var wait sync.WaitGroup
	count := b.N
	wait.Add(count)
	for i := 0; i < count; i++ {
		go func() {
			for range ch {
			}
			wait.Done()
		}()
	}
	for i := 0; i < count*1000; i++ {
		ch <- i
	}
	close(ch)
	wait.Wait()
}
func BenchmarkChan1ToN(b *testing.B) {
	ch := deque.NewChannel(deque.NewLess(make([]deque.Value, 100)))
	var wait sync.WaitGroup
	count := b.N
	wait.Add(count)
	for i := 0; i < count; i++ {
		go func() {
			var e error
			for {
				_, e = ch.PopFront()
				if e != nil {
					break
				}
			}
			wait.Done()
		}()
	}
	for i := 0; i < count*1000; i++ {
		ch.PushBack(i)
	}
	ch.Close()
	wait.Wait()
}
func BenchmarkGoChanNToN(b *testing.B) {
	ch := make(chan int, 100)
	var wait sync.WaitGroup
	count := b.N
	wait.Add(count)
	for i := 0; i < count; i++ {
		go func() {
			for range ch {
			}
			wait.Done()
		}()
	}
	var waitWrite sync.WaitGroup
	waitWrite.Add(count)
	for i := 0; i < count; i++ {
		go func() {
			for i := 0; i < 1000; i++ {
				ch <- i
			}
			waitWrite.Done()
		}()
	}
	go func() {
		waitWrite.Wait()
		close(ch)
	}()
	wait.Wait()
}
func BenchmarkChanNToN(b *testing.B) {
	ch := deque.NewChannel(deque.NewLess(make([]deque.Value, 100)))
	var wait sync.WaitGroup
	count := b.N
	wait.Add(count)
	for i := 0; i < count; i++ {
		go func() {
			var e error
			for {
				_, e = ch.PopBack()
				if e != nil {
					break
				}
			}
			wait.Done()
		}()
	}
	var waitWrite sync.WaitGroup
	waitWrite.Add(count)
	for i := 0; i < count; i++ {
		go func() {
			for i := 0; i < 1000; i++ {
				ch.PushBack(i)
			}
			waitWrite.Done()
		}()
	}
	go func() {
		waitWrite.Wait()
		ch.Close()
	}()
	wait.Wait()
}
