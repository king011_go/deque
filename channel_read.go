package deque

import (
	"errors"
	"sync"
)

// ErrChannelClosed channel closed
var ErrChannelClosed = errors.New(`channel closed`)

// ChannelRead like chan but use deque, pop will block if deque empty,push will return false if deque full
type ChannelRead struct {
	deque  Deque
	cond   *sync.Cond
	wait   int
	closed bool
}

// NewChannelRead .
func NewChannelRead(deque Deque) *ChannelRead {
	return &ChannelRead{
		deque: deque,
		cond:  sync.NewCond(&sync.Mutex{}),
	}
}

// Close close channel push and pop will return
func (c *ChannelRead) Close() (e error) {
	c.cond.L.Lock()
	if c.closed {
		e = ErrChannelClosed
	} else {
		c.closed = true
		if c.wait != 0 {
			c.cond.Broadcast()
		}
	}
	c.cond.L.Unlock()
	return
}

// PushBack append value to end
func (c *ChannelRead) PushBack(value Value) (ok bool, e error) {
	c.cond.L.Lock()
	if c.closed {
		e = ErrChannelClosed
	} else {
		ok = c.deque.PushBack(value)
		if ok {
			if c.wait != 0 {
				c.cond.Signal()
			}
		}
	}
	c.cond.L.Unlock()
	return
}

// PushFront insert value before of first
func (c *ChannelRead) PushFront(value Value) (ok bool, e error) {
	c.cond.L.Lock()
	if c.closed {
		e = ErrChannelClosed
	} else {
		ok = c.deque.PushFront(value)
		if ok {
			if c.wait != 0 {
				c.cond.Signal()
			}
		}
	}
	c.cond.L.Unlock()
	return
}

// PopFront if empty wait, else erase and return first value
func (c *ChannelRead) PopFront() (value Value, e error) {
	c.cond.L.Lock()
	for {
		if !c.deque.IsEmpty() {
			value = c.deque.PopFront()
			break
		} else if c.closed {
			e = ErrChannelClosed
			break
		}

		c.wait++      // add wait
		c.cond.Wait() // wait work
		c.wait--      // sub wait
	}
	c.cond.L.Unlock()
	return
}

// PopBack if empty wait, else erase and return last value
func (c *ChannelRead) PopBack() (value Value, e error) {
	c.cond.L.Lock()
	for {
		if !c.deque.IsEmpty() {
			value = c.deque.PopBack()
			break
		} else if c.closed {
			e = ErrChannelClosed
			break
		}

		c.wait++      // add wait
		c.cond.Wait() // wait work
		c.wait--      // sub wait
	}
	c.cond.L.Unlock()
	return
}
