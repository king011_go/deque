package deque_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/king011_go/deque"
)

func TestChannelWrite(t *testing.T) {
	q := deque.NewLess(make([]deque.Value, 3))
	ch := deque.NewChannel(q)
	ok, e := ch.PushBack(1)
	assert.NoError(t, e)
	assert.True(t, ok)
	ok, e = ch.PushBack(2)
	assert.NoError(t, e)
	assert.True(t, ok)
	ok, e = ch.PushBack(3)
	assert.NoError(t, e)
	assert.True(t, ok)
	go func() {
		time.Sleep(time.Millisecond)
		v, e := ch.PopFront()
		assert.NoError(t, e)
		assert.Equal(t, v, 1)
	}()
	ok, e = ch.PushBack(4)
	assert.NoError(t, e)
	assert.True(t, ok)
}
func TestChannelRead(t *testing.T) {
	q := deque.NewLess(make([]deque.Value, 3))
	ch := deque.NewChannel(q)
	go func() {
		time.Sleep(time.Millisecond)
		ch.PushBack(1)
	}()
	v, e := ch.PopBack()
	assert.NoError(t, e)
	assert.Equal(t, v, 1)

	for i := 0; i < 3; i++ {
		ok, e := ch.PushBack(i)
		assert.NoError(t, e)
		assert.True(t, ok)
	}

	assert.NoError(t, ch.Close())
	assert.Equal(t, ch.Close(), deque.ErrChannelClosed)

	ok, e := ch.PushBack(4)
	assert.Equal(t, e, deque.ErrChannelClosed)
	assert.False(t, ok)

	for i := 0; i < 3; i++ {
		v, e := ch.PopFront()
		assert.NoError(t, e)
		assert.Equal(t, v, i)
	}
	v, e = ch.PopFront()
	assert.Equal(t, e, deque.ErrChannelClosed)
	assert.Equal(t, v, nil)

}
