package deque

// A Array interface not support capacity adjustment
type Array interface {
	// Cap return capacity
	Cap() int
	// Len return length
	Len() int
	// IsEmpty if length == 0 return true, else return false.
	IsEmpty() bool
	// Foreach foreach call callback, if callback return not nil stop foreach and return this error
	Foreach(callback func(index int, value Value) error) (e error)
	// ReverseForeach like Foreach,but reverse
	ReverseForeach(callback func(index int, value Value) error) (e error)
	// At return value at index
	At(index int) (value Value, e error)
	// Set value on index
	Set(index int, value Value) (old Value, e error)
}

// A Queue inteface
type Queue interface {
	Array
	// Clear set length = 0 and all data = nil
	Clear()
	// PushBack append value to end
	PushBack(value Value) (ok bool)
	// Front if empty return nil, else return first value
	Front() (value Value)
	// PopFront if empty return nil, else erase and return first value
	PopFront() (value Value)
}

// A Deque inteface
type Deque interface {
	Queue
	// PushFront insert value before of first
	PushFront(value Value) (ok bool)
	// Back if empty return nil, else return last value
	Back() (value Value)
	// PopBack if empty return nil, else erase and return last value
	PopBack() (value Value)
}
