package deque

// A Full deque
type Full struct {
	opts fullOptions

	capacity int
	length   int

	reserve *Less
	data    []*Less
}

// NewFull create new Full deque
func NewFull(opt ...FullOption) *Full {
	opts := defaultFullOptions
	for _, o := range opt {
		o.apply(&opts)
	}

	opts.fix()
	capacity := opts.initCapacity
	return &Full{
		opts:     opts,
		capacity: capacity,
		data:     []*Less{NewLess(make([]Value, capacity))},
	}
}

// Cap return capacity
func (f *Full) Cap() int {
	return f.capacity
}

// Len return length
func (f *Full) Len() int {
	return f.length
}

// IsEmpty if length == 0 return true, else return false.
func (f *Full) IsEmpty() bool {
	return f.length == 0
}

// Foreach foreach call callback, if callback return not nil stop foreach and return this error
func (f *Full) Foreach(callback func(index int, value Value) error) (e error) {
	if f.length == 0 {
		return
	} else if f.length == 1 {
		less := f.data[0]
		e = callback(0, less.data[less.offset])
		return
	} else if len(f.data) == 1 {
		less := f.data[0]
		e = less.Foreach(callback)
		return
	}
	var index int
	for i := 0; i < len(f.data); i++ {
		e = f.data[i].Foreach(func(i int, value Value) error {
			e := callback(index, value)
			if e != nil {
				return e
			}
			index++
			return nil
		})
		if e != nil {
			break
		}
	}
	return
}

// ReverseForeach like Foreach,but reverse
func (f *Full) ReverseForeach(callback func(index int, value Value) error) (e error) {
	if f.length == 0 {
		return
	} else if f.length == 1 {
		less := f.data[0]
		e = callback(0, less.data[less.offset])
		return
	} else if len(f.data) == 1 {
		less := f.data[0]
		e = less.ReverseForeach(callback)
		return
	}
	index := f.length - 1
	for i := len(f.data) - 1; i >= 0; i-- {
		e = f.data[i].ReverseForeach(func(i int, value Value) error {
			e := callback(index, value)
			if e != nil {
				return e
			}
			index--
			return nil
		})
		if e != nil {
			break
		}
	}
	return
}

// Clear set length = 0 and all data = nil
func (f *Full) Clear() {
	if f.length == 0 {
		return
	}
	count := len(f.data)
	f.length = 0
	if count == 1 {
		f.reserve = nil
		f.data[0].Clear()
		return
	}
	var less = f.reserve
	f.reserve = nil
	for i := 0; i < count; i++ {
		if less == nil {
			less = f.data[i]
		} else if less.Cap() < f.data[i].Cap() {
			less = f.data[i]
		}
		f.data[i] = nil
	}
	f.data[0] = less
	f.data = f.data[:1]
	return
}

// PushBack append value to end
func (f *Full) PushBack(value Value) (ok bool) {
	if f.opts.limitCapacity > 0 && f.length == f.opts.limitCapacity {
		return
	}
	ok = true
	f.length++

	less := f.data[len(f.data)-1]
	if !less.PushBack(value) {
		if f.reserve != nil {
			less = f.reserve
			f.reserve = nil
		} else if f.opts.nodeCapacity > 0 {
			less = NewLess(make([]Value, f.opts.nodeCapacity))
		} else {
			if f.capacity < 1024 {
				less = NewLess(make([]Value, f.capacity*2))
			} else {
				less = NewLess(make([]Value, int(float64(f.capacity)*1.25)))
			}
		}
		if !less.PushBack(value) {
			panic(`Full PushBack error`)
		}
		f.data = append(f.data, less)
		f.capacity += less.Cap()
	}
	return
}

// PushFront insert value before of first
func (f *Full) PushFront(value Value) (ok bool) {
	if f.opts.limitCapacity > 0 && f.length == f.opts.limitCapacity {
		return
	}
	ok = true
	f.length++

	less := f.data[0]
	if !less.PushFront(value) {
		if f.reserve != nil {
			less = f.reserve
			f.reserve = nil
		} else if f.opts.nodeCapacity > 0 {
			less = NewLess(make([]Value, f.opts.nodeCapacity))
		} else {
			if f.capacity < 1024 {
				less = NewLess(make([]Value, f.capacity*2))
			} else {
				less = NewLess(make([]Value, int(float64(f.capacity)*1.25)))
			}
		}
		if !less.PushBack(value) {
			panic(`Full PushBack error`)
		}
		f.data = append(f.data, nil)
		for i := len(f.data) - 2; i >= 0; i-- {
			f.data[i+1] = f.data[i]
		}
		f.data[0] = less
		f.capacity += less.Cap()
	}
	return
}

// PopBack if empty return nil, else erase and return last value
func (f *Full) PopBack() (value Value) {
	if f.length == 0 {
		return
	}
	count := len(f.data)
	offset := count - 1
	less := f.data[offset]
	value = less.PopBack()
	if count > 1 && less.IsEmpty() {
		f.capacity -= less.Cap()
		f.data[offset] = nil
		f.data = f.data[:offset]
		if less.Cap() > f.data[offset-1].Cap() || f.opts.nodeCapacity > 0 {
			if f.reserve == nil {
				f.reserve = less
			} else if f.reserve.Cap() < less.Cap() {
				f.reserve = less
			}
		}
	}
	f.length--
	return
}

// PopFront if empty return nil, else erase and return first value
func (f *Full) PopFront() (value Value) {
	if f.length == 0 {
		return
	}
	count := len(f.data)
	less := f.data[0]
	value = less.PopFront()
	if count > 1 && less.IsEmpty() {
		f.capacity -= less.Cap()
		for i := 0; i < count-1; i++ {
			f.data[i] = f.data[i+1]
		}
		f.data[count-1] = nil
		f.data = f.data[:count-1]

		if less.Cap() > f.data[0].Cap() || f.opts.nodeCapacity > 0 {
			if f.reserve == nil {
				f.reserve = less
			} else if f.reserve.Cap() < less.Cap() {
				f.reserve = less
			}
		}
	}
	f.length--
	return
}

// At return value at index
func (f *Full) At(index int) (value Value, e error) {
	if index < 0 || index >= f.length {
		e = ErrOutOfRange
		return
	}
	count := len(f.data)
	for i := 0; i < count; i++ {
		less := f.data[i]
		if index >= less.length {
			index -= less.length
		} else {
			value, e = less.At(index)
			if e != nil {
				panic(e)
			}
			return
		}
	}
	panic(`not found`)
}

// Set value on index
func (f *Full) Set(index int, value Value) (old Value, e error) {
	if index < 0 || index >= f.length {
		e = ErrOutOfRange
		return
	}
	count := len(f.data)
	for i := 0; i < count; i++ {
		less := f.data[i]
		if index >= less.length {
			index -= less.length
		} else {
			index, e = less.at(index)
			if e != nil {
				panic(e)
			}
			less.data[index] = value
			return
		}
	}
	panic(`not found`)
}

// Back if empty return nil, else return last value
func (f *Full) Back() (value Value) {
	if f.length == 0 {
		return
	}
	count := len(f.data)
	offset := count - 1
	less := f.data[offset]
	value = less.Back()
	return
}

// Front if empty return nil, else return first value
func (f *Full) Front() (value Value) {
	if f.length == 0 {
		return
	}
	less := f.data[0]
	value = less.Front()
	return
}
