package deque_test

import (
	"container/list"
	"testing"

	"gitlab.com/king011_go/deque"
)

func BenchmarkListQueueInt(b *testing.B) {
	q := list.New()
	for i := 0; i < b.N; i++ {
		q.PushBack(1)
		q.PushBack(1)
		q.Remove(q.Front())
	}
}

func BenchmarkSliceQueueInt(b *testing.B) {
	var q []int
	for i := 0; i < b.N; i++ {
		q = append(q, 1)
		q = append(q, 1)
		q = q[1:]
	}
}

func BenchmarkFullQueueInt(b *testing.B) {
	q := deque.NewFull()
	for i := 0; i < b.N; i++ {
		q.PushBack(1)
		q.PushBack(1)
		q.PopFront()
	}
}

func BenchmarkListQueueBytes(b *testing.B) {
	q := list.New()
	for i := 0; i < b.N; i++ {
		q.PushBack([1024]byte{})
		q.PushBack([1024]byte{})
		q.Remove(q.Front())
	}
}

func BenchmarkSliceQueueBytes(b *testing.B) {
	var q [][1024]byte
	for i := 0; i < b.N; i++ {
		q = append(q, [1024]byte{})
		q = append(q, [1024]byte{})
		q = q[1:]
	}
}
func BenchmarkFullQueueBytes(b *testing.B) {
	q := deque.NewFull()
	for i := 0; i < b.N; i++ {
		q.PushBack([1024]byte{})
		q.PushBack([1024]byte{})
		q.PopFront()
	}
}
