package deque

import (
	"errors"
)

// ErrOutOfRange .
var ErrOutOfRange = errors.New(`index out of range`)

// Value data any
type Value interface{}

// A Less deque
type Less struct {
	length int
	offset int
	data   []Value
}

// NewLess create new Less deque
func NewLess(data []Value) *Less {
	return &Less{
		data: data,
	}
}

// Cap return capacity
func (l *Less) Cap() int {
	return len(l.data)
}

// Len return length
func (l *Less) Len() int {
	return l.length
}

// IsEmpty if length == 0 return true, else return false.
func (l *Less) IsEmpty() bool {
	return l.length == 0
}

// Foreach foreach call callback, if callback return not nil stop foreach and return this error
func (l *Less) Foreach(callback func(index int, value Value) error) (e error) {
	if l.length == 0 {
		return
	} else if l.length == 1 {
		e = callback(0, l.data[l.offset])
		return
	}
	var value Value
	for i := 0; i < l.length; i++ {
		value, e = l.At(i)
		if e != nil {
			panic(e)
		}
		e = callback(i, value)
		if e != nil {
			break
		}
	}
	return
}

// ReverseForeach like Foreach,but reverse
func (l *Less) ReverseForeach(callback func(index int, value Value) error) (e error) {
	if l.length == 0 {
		return
	} else if l.length == 1 {
		e = callback(0, l.data[l.offset])
		return
	}
	var value Value
	for i := l.length - 1; i >= 0; i-- {
		value, e = l.At(i)
		if e != nil {
			panic(e)
		}
		e = callback(i, value)
		if e != nil {
			break
		}
	}
	return
}

// Clear set length = 0 and all data = nil
func (l *Less) Clear() {
	if l.length == 0 {
		l.offset = 0
		return
	} else if l.length == 1 {
		l.data[l.offset] = nil
		l.offset = 0
		l.length = 0
		return
	}
	var index int
	var e error
	for i := 0; i < l.length; i++ {
		index, e = l.at(i)
		if e != nil {
			panic(e)
		}
		l.data[index] = nil
	}
	l.offset = 0
	l.length = 0
	return
}

// PushBack append value to end
func (l *Less) PushBack(value Value) (ok bool) {
	end := len(l.data)
	if l.length == end {
		return
	}
	ok = true
	l.length++
	index, e := l.at(l.length - 1)
	if e != nil {
		panic(e)
	}
	l.data[index] = value
	return
}

// PushFront insert value before of first
func (l *Less) PushFront(value Value) (ok bool) {
	end := len(l.data)
	if l.length == end {
		return
	}
	ok = true
	l.length++
	if l.offset == 0 {
		l.offset = end - 1
	} else {
		l.offset--
	}
	l.data[l.offset] = value
	return
}

// PopBack if empty return nil, else erase and return last value
func (l *Less) PopBack() (value Value) {
	if l.length == 0 {
		return
	}
	index, e := l.at(l.length - 1)
	if e != nil {
		panic(e)
	}
	value = l.data[index]
	l.data[index] = nil
	l.length--
	return
}

// PopFront if empty return nil, else erase and return first value
func (l *Less) PopFront() (value Value) {
	if l.length == 0 {
		return
	}
	value = l.data[l.offset]
	l.data[l.offset] = nil
	l.length--
	l.offset++
	end := len(l.data)
	if l.offset == end {
		l.offset = 0
	}
	return
}

func (l *Less) at(index int) (int, error) {
	if index < 0 || index >= l.length {
		return 0, ErrOutOfRange
	}
	if l.offset != 0 {
		index += l.offset
		end := len(l.data)
		if index >= end {
			index -= end
		}
	}
	return index, nil
}

// At return value at index
func (l *Less) At(index int) (value Value, e error) {
	index, e = l.at(index)
	if e != nil {
		return
	}
	value = l.data[index]
	return
}

// Set value on index
func (l *Less) Set(index int, value Value) (old Value, e error) {
	index, e = l.at(index)
	if e != nil {
		return
	}
	old = l.data[index]
	l.data[index] = value
	return
}

// Back if empty return nil, else return last value
func (l *Less) Back() (value Value) {
	if l.length == 0 {
		return
	}
	value, e := l.At(l.length - 1)
	if e != nil {
		panic(e)
	}
	return
}

// Front if empty return nil, else return first value
func (l *Less) Front() (value Value) {
	if l.length == 0 {
		return
	}
	value = l.data[l.offset]
	return
}
