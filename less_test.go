package deque_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/king011_go/deque"
)

func equals(q deque.Deque, value ...interface{}) bool {
	count := q.Len()
	if count != len(value) {
		return false
	}
	for i := 0; i < count; i++ {
		v, e := q.At(i)
		if e != nil {
			panic(e)
		}
		if v != value[i] {
			return false
		}
	}
	return true
}
func TestLess(t *testing.T) {
	// init
	q := deque.NewLess(make([]deque.Value, 3))
	assert.Equal(t, q.Len(), 0)
	assert.Equal(t, q.Cap(), 3)
	assert.True(t, q.IsEmpty())

	// push
	assert.True(t, q.PushBack(1))
	assert.Equal(t, q.Len(), 1)
	assert.Equal(t, q.Cap(), 3)
	assert.True(t, equals(q, 1))
	assert.False(t, q.IsEmpty())

	assert.True(t, q.PushFront(2))
	assert.Equal(t, q.Len(), 2)
	assert.Equal(t, q.Cap(), 3)
	assert.True(t, equals(q, 2, 1))
	assert.False(t, q.IsEmpty())

	assert.True(t, q.PushBack(3))
	assert.Equal(t, q.Len(), 3)
	assert.Equal(t, q.Cap(), 3)
	assert.True(t, equals(q, 2, 1, 3))
	assert.False(t, q.IsEmpty())

	assert.False(t, q.PushBack(4))
	assert.True(t, equals(q, 2, 1, 3))

	// pop
	assert.Equal(t, 2, q.PopFront())
	assert.True(t, equals(q, 1, 3))
	assert.Equal(t, q.Len(), 2)

	assert.Equal(t, 3, q.PopBack())
	assert.True(t, equals(q, 1))
	assert.Equal(t, q.Len(), 1)

	// other
	q.Clear()
	assert.Equal(t, q.Len(), 0)
	assert.Equal(t, q.Cap(), 3)
	assert.True(t, q.IsEmpty())

	// foreach PushBack
	for i := 0; i < 100; i++ {
		if i > 2 {
			assert.False(t, q.PushBack(i))

			assert.Equal(t, q.Len(), 3)
			assert.Equal(t, q.PopFront(), i-3)
			assert.Equal(t, q.Len(), 2)
		}
		assert.True(t, q.PushBack(i))
		if i%3 == 2 {
			vs := []interface{}{
				i - 2, i - 1, i,
			}
			v0, e := q.At(0)
			assert.NoError(t, e)
			assert.Equal(t, v0, vs[0])
			v1, e := q.At(1)
			assert.NoError(t, e)
			assert.Equal(t, v1, vs[1])
			v2, e := q.At(2)
			assert.NoError(t, e)
			assert.Equal(t, v2, vs[2])
			count := 0
			assert.NoError(t, q.Foreach(func(index int, val deque.Value) error {
				assert.Equal(t, count, index)
				count++
				assert.Equal(t, val, vs[index])
				return nil
			}))
			assert.Equal(t, count, 3)
			count = 0
			assert.NoError(t, q.ReverseForeach(func(index int, val deque.Value) error {
				assert.Equal(t, count, 2-index)
				count++
				assert.Equal(t, val, vs[index])
				return nil
			}))
			assert.Equal(t, count, 3)
		}
	}
	q.Clear()
	assert.True(t, q.IsEmpty())
	// foreach PushFront
	for i := 0; i < 100; i++ {
		if i > 2 {
			assert.False(t, q.PushFront(i))

			assert.Equal(t, q.Len(), 3)
			assert.Equal(t, q.PopBack(), i-3)
			assert.Equal(t, q.Len(), 2)
		}
		assert.True(t, q.PushFront(i))
		if i%3 == 2 {
			vs := []interface{}{
				i, i - 1, i - 2,
			}
			v0, e := q.At(0)
			assert.NoError(t, e)
			assert.Equal(t, v0, vs[0])
			v1, e := q.At(1)
			assert.NoError(t, e)
			assert.Equal(t, v1, vs[1])
			v2, e := q.At(2)
			assert.NoError(t, e)
			assert.Equal(t, v2, vs[2])
			count := 0
			assert.NoError(t, q.Foreach(func(index int, val deque.Value) error {
				assert.Equal(t, count, index)
				count++
				assert.Equal(t, val, vs[index])
				return nil
			}))
			assert.Equal(t, count, 3)
			count = 0
			assert.NoError(t, q.ReverseForeach(func(index int, val deque.Value) error {
				assert.Equal(t, count, 2-index)
				count++
				assert.Equal(t, val, vs[index])
				return nil
			}))
			assert.Equal(t, count, 3)
		}
	}
}
