package deque

var defaultFullOptions = fullOptions{}

type fullOptions struct {
	initCapacity  int
	limitCapacity int
	nodeCapacity  int
}

func (f *fullOptions) fix() {
	if f.limitCapacity > 0 {
		if f.nodeCapacity > f.limitCapacity {
			f.nodeCapacity = f.limitCapacity
		}
		if f.nodeCapacity > 0 {
			if f.initCapacity > f.nodeCapacity {
				f.initCapacity = f.nodeCapacity
			}
		} else if f.initCapacity > f.limitCapacity {
			f.initCapacity = f.limitCapacity
		}
	} else if f.nodeCapacity > 0 {
		if f.initCapacity > f.nodeCapacity {
			f.initCapacity = f.nodeCapacity
		}
	}
	if f.initCapacity < 1 {
		f.initCapacity = 64
	}
	if f.nodeCapacity > 0 {
		if f.initCapacity > f.nodeCapacity {
			f.initCapacity = f.nodeCapacity
		}
	} else if f.limitCapacity > 0 {
		if f.initCapacity > f.limitCapacity {
			f.initCapacity = f.limitCapacity
		}
	}
}

// A FullOption sets options .
type FullOption interface {
	apply(*fullOptions)
}
type funcFullOption struct {
	f func(*fullOptions)
}

func (fdo *funcFullOption) apply(do *fullOptions) {
	fdo.f(do)
}
func newFuncFullOption(f func(*fullOptions)) *funcFullOption {
	return &funcFullOption{
		f: f,
	}
}

// WithFullLimitCapacity set limit capacity,if capacity < 1 not limit
func WithFullLimitCapacity(capacity int) FullOption {
	return newFuncFullOption(func(opt *fullOptions) {
		opt.limitCapacity = capacity
	})
}

// WithFullInitCapacity set init array data capacity
func WithFullInitCapacity(capacity int) FullOption {
	return newFuncFullOption(func(opt *fullOptions) {
		opt.initCapacity = capacity
	})
}

// WithFullNodeCapacity set node limit capacity,if capacity < 1 not limit
func WithFullNodeCapacity(capacity int) FullOption {
	return newFuncFullOption(func(opt *fullOptions) {
		opt.nodeCapacity = capacity
	})
}
